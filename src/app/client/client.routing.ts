import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {ClientComponent} from './client.component';

const CLIENT_CHILD_ROUTES: Routes = [
  {
    path: '', component: ClientComponent, children: []
  }
];

@NgModule({
  imports: [RouterModule.forChild(CLIENT_CHILD_ROUTES)],
  exports: [RouterModule]
})
export class ClientRouting {
}




