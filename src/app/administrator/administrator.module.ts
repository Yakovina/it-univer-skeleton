import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdministratorComponent } from './administrator.component';
import {AppSharedModule} from '../shared/app-shared.module';
import {AdministratorRouting} from './administrator.routing';

@NgModule({
  imports: [
    CommonModule, AppSharedModule, AdministratorRouting
  ],
  declarations: [AdministratorComponent],
  providers: []
})
export class AdministratorModule { }
